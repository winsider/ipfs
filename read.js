import * as IPFS from 'ipfs-core'

const ipfs = await IPFS.create({repo: "subscriber"})
const decoder = new TextDecoder()
const topic = "vulcan607"

console.log('reading file...');

for await (const buf of ipfs.get('QmYamCxKZUgMM4MKYnA3dNQb11ukPUEWqyKeYzopBe2Gye')) {
    console.log(buf);
}

console.log('subscribing...');

function subscribeTime(msg)
{
    console.log(decoder.decode(msg.data));
}

ipfs.pubsub.subscribe(topic, subscribeTime);

